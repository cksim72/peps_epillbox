/**
 * auth-factory.js
 */

(function () {

angular.module('PMSApp').factory('AuthFactory', ['$q', '$timeout', '$http', '$state',
        function ($q, $timeout, $http, $state) {

            // create user variable
            var user = null;
            // return available functions for use in the controllers
            return ({
                isLoggedIn: isLoggedIn,
                getUserStatus: getUserStatus,
                login: login,
                logout: logout,
                register: register,
                resetPassword: resetPassword,
                changePassword: changePassword
            });

            function isLoggedIn() {
                if(user) {
                    return true;
                } else {
                    return false;
                }
            }

            function isAdmin() {
                console.log("isAdmin? user: ",user);
            }

            function getUserStatus(callback) {
                $http.get('/status/user')
                // handle success
                    .then(function (data) {
                        var authResult = JSON.stringify(data);
                        if(data["data"] != ''){
                            user = true;
                            callback(user);
                        } else {
                            user = false;
                            callback(user);
                        }
                    });
            }

            // userProfile: JSON with user_id and password
            function login(userProfile) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/login', userProfile)
                // handle success
                    .then(function (data) {
                        var status = data.status;
                        if(status == 200){
                            getUserStatus(function(result){
                                if(result){
                                    console.log("getUserStatus:", result)
                                    deferred.resolve();
                                    $state.go('supervisor');
                                }else{
                                    deferred.reject();
                                    $state.go('signIn');
                                }
                            });
                        } else {
                            user = false;
                            deferred.reject();
                        }
		            })
                    // handle error
                    .catch(function (data) {
                        user = false;
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function logout() {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a get request to the server
                $http.get('/logout')
                // handle success
                    .then(function (data) {
                        user = false;
                        deferred.resolve();
                        console.log("Logout complete");
                    })
                    // handle error
                    .catch(function (data) {
                        user = false;
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function resetPassword(userProfile) {
                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/reset-password',
                    userProfile)
                // handle success
                    .then(function (data) {
                        var status = data.status;
                        if(status){
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .catch(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
            }

            function changePassword(userProfile) {
                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/change-password',
                    userProfile)
                // handle success
                    .then(function (data) {
                        var status = data.status;
                        if(status){
                            console.log("--> change password Good !");
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .catch(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
            }

            function register(username, password, firstName, lastName) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/register',
                    {username: username, password: password, firstName: firstName, lastName: lastName})
                // handle success
                    .then(function (data) {
                        var status = data.status;
                        if(status){
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    })
                    // handle error
                    .catch(function (data) {
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;
            }
        }]);
})();
