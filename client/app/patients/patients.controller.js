/*
    patients.controller.js
*/

(function(){

    angular
        .module("PMSApp")
        .controller("PatientsCtrl", ["PatientsAPI", "$q", "AuthFactory", "$rootScope", "$state", "$location", "$scope", PatientsCtrl]);

    function PatientsCtrl(PatientsAPI, $q, AuthFactory, $rootScope, $state, $location, $scope){
        var vm = this;

        vm.patientsList = [];


        if ($scope.$resolve.authenticated == false) {
                    $state.go('signIn');
        }
        else {

            PatientsAPI.list().then(function(data){
                vm.patientsList=data;
                console.log("Get All Patients: ", vm.patientsList);
            });
        }

        /***********************************/
        vm.patient = {
            id: "",
            email: "",
            password: "",
            first_name: "",
            last_name: "",
            gender: "F",
            birth_date: "",
            address: "",
            contact: "",
            nurse_id: "S8012056Z",
            doctor_id: "S7012056Z",
            created_by_id: "S8012056Z"
        };

        // vm.isAgeValid = function () {
        //     var date = new Date(vm.patient.birth_date);
        //     date.setFullYear(date.getFullYear() + 18);
        //     return date <= new Date();
        // };

        vm.photoClick = function () {
            console.log("Photo clicked");
        }

        vm.addPatient = function () {
            // if (vm.isAgeValid() == false) {
            //     return;
            // }

            vm.addPatient_successful=true;

            PatientsAPI.insert(vm.patient)
                .then(function () {
                    // alert("Patient successfully added");
                    // $state.go("supervisor");
                    console.log("Patient successfully added");
                })
                .catch(function () {
                    alert("Error creating Patient record");
                });
        };

        vm.exit = function () {
            console.log("Return to Home");
            $state.go("supervisor");
        }

    }
})();
