(function () {
    angular
        .module("PMSApp", [
            "ui.router",
            "ngSanitize",
            "uiRouterStyles",
            "ngMessages",
            "data-table"
        ]);
})();
