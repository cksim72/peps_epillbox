/*
    nurses.controller.js
*/

(function(){

    angular
        .module("PMSApp")
        .controller("NursesCtrl", ["NursesAPI", "$q", "AuthFactory", "$rootScope", "$state", "$location", "$scope", NursesCtrl]);

    function NursesCtrl(NursesAPI,$q, AuthFactory, $rootScope, $state, $location, $scope){
        var vm = this;

        vm.sociallogins = [];
        vm.nursesList = [];
        vm.data = [];

        if ($scope.$resolve.authenticated ==  false)
        {
            $state.go("signIn");
        }
        else {

            NursesAPI.list().then(function(data){
            vm.nursesList=data;
            console.log("Get All Nurses: ", vm.nursesList);
            });

        }

        vm.edit = function(id) {
            console.log("ID: ", id);
            // $state.go("editnurse", {'nurseId' : id});

        }
        
        vm.save = function(nurse) {

            NursesAPI.save(vm.nursesList[index])
            .then(function(data){
                console.log("Nurse record updated successfully");
            });

        }

        vm.delete = function(index) {
            console.log("Delete click: $index ", index);

            NursesAPI.remove(vm.nursesList[index])
            .then(function(data){
                console.log("Nurse record deleted successfully");

                NursesAPI.list().then(function(data){
                    vm.nursesList=data;
                    console.log("Get All Nurses: ", vm.nursesList);
                });
            });
        }

    }

})();
