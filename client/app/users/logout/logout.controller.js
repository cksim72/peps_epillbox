/**
 * logout.controller.js
 */
(function () {
  angular
        .module("PMSApp")
        .controller("LogoutCtrl", ["$state", "AuthFactory", LogoutCtrl]);

    function LogoutCtrl($state, AuthFactory){
        var vm = this;

        vm.msg="";

        vm.logout = function () {
            AuthFactory.logout()
                .then(function () {
                    $state.go("signIn");
                }).catch(function () {
                console.error("Error logging off !");
            });
        };
    }
})();
