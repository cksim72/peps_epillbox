/*
    login.controller.js
*/

(function () {
    angular
        .module("PMSApp")
        .controller("LoginCtrl", ["$state", "AuthFactory", LoginCtrl]);

    function LoginCtrl($state, AuthFactory){
        var vm = this;

        vm.user = {id: "", password: ""};

        vm.loginFail=false;
        vm.errMsg="";

        vm.login = function () {
            vm.user.id=vm.user.id.toUpperCase();
            console.log("Login details:", vm.user);
            vm.loginFail=false;
            vm.errMsg="";

             AuthFactory.login(vm.user)
                .then(function () {
                    vm.errMsg="";
                    if(AuthFactory.isLoggedIn()){
                        vm.user.id = "";
                        vm.user.password = "";
                        vm.loginFail=false;
                        $state.go("supervisor");
                    }else{
                        vm.loginFail=true;
                        vm.errMsg="Invalid ID or password."
                    }
                }).catch(function () {
                    console.error("Error logging on !");
                    vm.loginFail=true;
                    vm.errMsg="Invalid ID or password."
                });
        }
    }
})();