/*
    patients-api.service.js
 */
(function () {
    angular.module("PMSApp")
        .service("PatientsAPI", ["$http", "$q", PatientsAPI]);

    function PatientsAPI($http, $q) {
        var vm = this;

        vm.list = function (callback){
            var defer = $q.defer();
            $http.get("/api/patients")
                .then(function(result) {
                    console.log("Patients getAll result: ", result.data)
                    defer.resolve(result.data);

                })
                .catch(function(error) {
                    defer.reject(error);
            });
            return defer.promise;
        };

        vm.search = function (patientId) {
            var defer = $q.defer();
            $http.get("/api/patients/" + patientId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (patient) {
            var defer = $q.defer();
            $http.put("/api/patients/" + patient.id, patient)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (patient) {
            var defer = $q.defer();
            $http.delete("/api/patients/" + patient.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.insert = function (patient) {
            var defer = $q.defer();
            $http.post("/api/patients/", patient)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
