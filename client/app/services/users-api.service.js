/*
    users-api.service.js
 */
(function () {
    angular.module("PMSApp")
        .service("UserAPI", ["$http", "$q", UserAPI]);

    function UserAPI($http, $q) {
        var self = this;

        self.getLocalProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/nurses/view-profile")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        self.getAllSocialLoginsProfile = function (callback){
            var defer = $q.defer();
            $http.get("/api/user/social/profiles")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        ///////////////////////////////////////////////////////
        self.getLocalProfileToken = function (resetToken){
            var defer = $q.defer();
            $http.get("/api/user/get-profile-token?resetToken=" + resetToken)
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                    defer.reject(error);
                });
            return defer.promise;
        };

        self.changePasswordToken = function(data) {
            return $http({
                method: 'POST',
                url: '/api/user/change-passwordToken',
                data: data
            }).then(function (result) {
                return result.data;
            });    
        }

        self.modify = function (nurse) {
            var defer = $q.defer();
            $http.put("/api/nurses/" + nurse.id, nurse)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
