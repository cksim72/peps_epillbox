/*
    nurses-api.service.js
 */
(function () {
    angular.module("PMSApp")
        .service("NursesAPI", ["$http", "$q", NursesAPI]);

    function NursesAPI($http, $q) {
        var vm = this;

        vm.list = function (callback){
            var defer = $q.defer();
            $http.get("/api/nurses")
                .then(function(result){
                    console.log("Nurses getAll result: ", result.data)
                    defer.resolve(result.data);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        vm.search = function (nurseID) {
            var defer = $q.defer();
            $http.get("/api/nurses/" + nurseID)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (nurse) {
            var defer = $q.defer();
            $http.put("/api/nurses/" + nurse.id, nurse)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (nurse) {
            var defer = $q.defer();
            $http.delete("/api/nurses/" + nurse.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
