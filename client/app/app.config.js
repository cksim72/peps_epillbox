(function () {
    angular
        .module("PMSApp")
        .config(pmsAppConfig);
    pmsAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function pmsAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("signIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login/login.html",
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl',
                data: {
                            css: "../app/users/styles/login.css"
                        }
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html"
                    }
                },
                controller: 'ResetPasswordCtrl',
                controllerAs: 'ctrl'
            })
            ////////////////////////////////////////////////////////////////////////////
            .state("ChangeNewpassword", {
                url: "/ChangeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html"
                    }
                },
                controller: 'ChangeNewPasswordCtrl',
                controllerAs: 'ctrl'
            })
            ////////////////////////////////////////////////////////////////////////////
            // .state("signUp", {
            //     url: "/signUp",
            //     views: {
            //         "content": {
            //             templateUrl: "../app/users/register1.html"
            //         }
            //     },
            //     controller: 'RegisterCtrl',
            //     controllerAs: 'ctrl'
            // })
            .state("logout", {
                url: "/logout",
                views: {
                    "content": {
                        templateUrl: "../app/users/logout/logout.html"
                    }
                },
                controller: 'LogoutCtrl',
                controllerAs: 'ctrl'
            })
            .state("nurses", {
                url: "/nurses",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/nurses.html"
                    }
                },
                controller: 'NursesCtrl',
                controllerAs: 'ctrl',
                data: {
                            css: "../app/assets/css/nurses.css"
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                }
            })
            .state("patients", {
                url: "/patients",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/patients.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PatientsCtrl',
                controllerAs: 'ctrl',
                data: {
                            css: "../app/assets/css/addpage.css"
                        }
            })
            .state("profile", {
                url: "/profile",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ProfileCtrl',
                controllerAs: 'ctrl',
                data: {
                            css: "../app/users/styles/profile.css"
                        }
            })
            .state("editprofile", {
                url: "/editprofile",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/edit_profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?", AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ProfileCtrl',
                controllerAs: 'ctrl',
                data: {
                            css: "../app/assets/css/addpage.css"
                        }
            })
            // .state("ChangePassword", {
            //     url: "/ChangePassword",
            //     views: {
            //         "nav": {
            //             templateUrl: "../app/protected/navigation.html"
            //         },
            //         "content": {
            //             templateUrl: "../app/protected/changePassword.html"
            //         }
            //     },
            //     resolve: {
            //         authenticated: function (AuthFactory) {
            //             console.log("authenticated ?");
            //             console.log(AuthFactory.isLoggedIn());
            //             return AuthFactory.isLoggedIn();
            //         }
            //     },
            //     controller: 'ChangePasswordCtrl',
            //     controllerAs: 'ctrl'
            // })
            .state('supervisor', {
                url: '/home',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/home.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'HomePageAppCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/signIn");

    }
})();
