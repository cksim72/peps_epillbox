/*
    profile.controller.js
*/

(function(){

    angular
        .module("PMSApp")
        .controller("ProfileCtrl", ["UserAPI", "$q", "AuthFactory", "$rootScope", "$state", "$location", "$scope", ProfileCtrl]);

    function ProfileCtrl(UserAPI,$q, AuthFactory, $rootScope, $state, $location, $scope){
        var vm = this;

        vm.sociallogins = [];
        vm.nurse = {};

        vm.editNurse_successful=false;

        if ($scope.$resolve.authenticated ==  false)
        {
            $state.go("signIn");
        }
        else {

            UserAPI.getLocalProfile().then(function(result){
                console.log("Get user local profile: ", result.data);
                vm.nurse = result.data;
                vm.nurse.birth_date = new Date(vm.nurse.birth_date);
            });
        }

        // AuthFactory.getUserStatus(function(result){
        //     vm.isUserLogon = result;
        // });

        // vm.isUserLogon = AuthFactory.isLoggedIn();
        // $rootScope.$watch('user', function() {
        // 	console.log(vm.isUserLogon);
        //         console.log($state.$current.url);
        //     if($state.$current.url != "" && !vm.isUserLogon){
        //         $state.go('signIn');
        //     }
    	// });
 
        // var defer = $q.defer();
        // vm.err = null;

        // vm.isActive = function (viewLocation) {
        //     //console.log(viewLocation);
        //     console.log($location.path());
        //     return viewLocation === $location.path();
        // };

        vm.updateProfile = function () {
            
            UserAPI.modify(vm.nurse)
                .then(function(result){
                    vm.editNurse_successful=true;
                    console.log("Modify user/nurse profile: ", result);
                    console.log("Profile successfully updated");
                })
                .catch(function () {
                    alert("Error updating profile.");
                });
        }

        vm.exit = function () {
            console.log("Return to Home");
            $state.go("supervisor");
        }

        /*** Not using social profile yet ***/
        // UserAPI.getAllSocialLoginsProfile().then(function(result){
        //     vm.sociallogins = result.data;
        // });
    }
})();


