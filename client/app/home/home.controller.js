(function () {
    angular
        .module("PMSApp")
        .controller("HomePageAppCtrl", ["$q", "AuthFactory", "$rootScope", "$state", "$location", "$scope", HomePageAppCtrl]);

    function HomePageAppCtrl($q, AuthFactory, $rootScope, $state, $location, $scope){
        var vm = this;

        if ($scope.$resolve.authenticated == false) {
            $state.go('signIn');
        }
 
    }

})();