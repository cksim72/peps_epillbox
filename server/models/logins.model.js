// Model for Login table

var Sequelize = require("sequelize");

module.exports = function(database) {
    return database.define('logins', {
        id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references: {
                    model: 'nurses',
                    key: 'id'
                }
        },
        password: {
            type: Sequelize.STRING(127),
            allowNull: false
        },
        last_login: {
            type: Sequelize.DATE,
            allowNull: true
        },
        last_logout: {
            type: Sequelize.DATE,
            allowNull: true
        },
        created_by_id: {
            type: Sequelize.STRING(16),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        tableName: 'logins',
        timestamps: true
    });
};