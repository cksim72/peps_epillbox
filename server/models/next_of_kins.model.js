// Model for Next-Of-Kin (NOK) table

var Sequelize = require("sequelize");

module.exports = function(database) {
    return database.define('next_of_kins', {
        id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true
        },
        first_name: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M','F'),
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        address: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        contact: {
            type: Sequelize.STRING(16),
            allowNull: false
        },
        patient_id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references : {
                model: 'patients',
                key: 'id'
            }
        },
        relationship: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        created_by_id: {
            type: Sequelize.STRING(16),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        tableName: 'next_of_kins',
        timestamps: false
    });
};