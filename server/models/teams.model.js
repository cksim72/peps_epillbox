// Model for Team table

var Sequelize = require("sequelize");

module.exports = function(database) {
    return database.define('teams', {
        name: {
            type: Sequelize.STRING(32),
            allowNull: false,
            primaryKey: true
        },
        nurse_id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references: {
                    model: 'nurses',
                    key: 'id'
                }
        },
        role: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        created_by_id: {
            type: Sequelize.STRING(16),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        tableName: 'teams',
        timestamps: false
    });
};
