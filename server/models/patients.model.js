// Model for patient table

var Sequelize = require("sequelize");

module.exports = function(database) {
    return database.define('patients', {
        id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M','F'),
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        address: {
            type: Sequelize.STRING(64),
            allowNull: false
        },
        contact: {
            type: Sequelize.STRING(16),
            allowNull: false
        },
        nurse_id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references : {
                model: 'nurses',
                key: 'id'
            }
        },
        doctor_id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references: {
                    model: 'doctors',
                    key: 'id'
                }
        },
        created_by_id: {
            type: Sequelize.STRING(16),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        tableName: 'patients',
        timestamps: true
    });
};