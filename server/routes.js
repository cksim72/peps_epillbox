'use strict';

// var AWSController = require("./api/aws/aws.controller");

var PatientsController = require("./api/patients/patients.controller");
var NursesController = require("./api/nurses/nurses.controller");
// var TeamsController = require("./api/teams/teams.controller");
// var PillDispensersController = require("./api/pilldispensers/pilldispensers.controller");
// var NOKsController = require("./api/next_of_kins/next_of_kins.controller");
// var LoginsController = require("./api/logins/logins.controller");


var express = require("express");
var config = require("./config");

const API_PILLDISPENSERS_URI = "/api/pilldispensers";
const API_NEXT_OF_KINS_URI = "/api/noks";
const API_PATIENTS_URI = "/api/patients";
const API_NURSES_URI = "/api/nurses";
const API_TEAMS_URI = "/api/teams";
const API_DOCTORS_URI = "/api/doctors";

// const API_AWS_URI = "/api/aws";

const TEAM_LEADER_HOME_PAGE = "/#!/supervisor";
const NURSE_HOME_PAGE = "/#!/profile";
const SIGNIN_PAGE = "/#!/signIn";


module.exports = function (app, passport) {

    // Patients API
    app.get(API_PATIENTS_URI, isAuthenticated, PatientsController.list);        // List All
    app.post(API_PATIENTS_URI, isAuthenticated, PatientsController.create);     // Create 

    // Nurses API
    app.get(API_NURSES_URI, isAuthenticated, NursesController.list);            // List All
    app.put("/api/nurses/:id", isAuthenticated, NursesController.update);       // Update
    app.delete("/api/nurses/:id", isAuthenticated, NursesController.remove);    // Delete

/*
    // Teams API
    app.get(API_TEAMS_URI, TeamsController.list);                               //List All

    // Pill Dispensers API
    app.get(API_PILLDISPENSERS_URI, PillDispensersController.list);             //List All

    // NOKs API
    app.get(API_NEXT_OF_KINS_URI, NOKsController.list);                         //List All

    //  Logins API
    app.get(API_LOGINS_URI, LoginsController.list);                             //List All
    

*/    
    app.get("/api/nurses/view-profile", isAuthenticated, NursesController.profile);

    // Static path
    app.use(express.static(__dirname + "/../client/"));

    // app.post("/change-password", isAuthenticated, UserController.changePasswd);
    // app.get("/api/user/get-profile-token", UserController.profileToken);
    // app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    //unprotected end point
    // app.post('/register', UserController.register);
    // app.post("/reset-password", UserController.resetPasswd);
    
    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + TEAM_LEADER_HOME_PAGE);
    });

    app.get('/protected/', isAuthenticated, function(req, res) {
        if(req.user == null){
            res.redirect(SIGNIN_PAGE);
        }
        // res.redirect('..' + TEAM_LEADER_HOME_PAGE);
    });

    app.post("/login", passport.authenticate("local", {
        successRedirect: NURSE_HOME_PAGE,
        failureRedirect: "/",
        failureFlash : true
    }));

    app.get("/status/user", function (req, res) {
        var status = "";
        if(req.user) {
            status = req.user.id;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    app.get("/logout", function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }

    app.use(function(req, res, next){
        if(req.user == null){
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};
