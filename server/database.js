var Sequelize = require('sequelize');
var config = require("./config");

console.log(config.mysql);

var database = new Sequelize(config.mysql, {
    pool: {
        max: 5,
        min: 1,
        idle: 10000
    }
});

var Logins = require("./models/logins.model.js")(database);
var Patients = require("./models/patients.model.js")(database);
var NextOfKins = require("./models/next_of_kins.model.js")(database);
var Nurses = require("./models/nurses.model.js")(database);
var Teams = require("./models/teams.model.js")(database);
var PillDispensers = require("./models/pilldispensers.model.js")(database);
var Doctors = require("./models/doctors.model.js")(database);

// BEGIN: MYSQL RELATIONS

Nurses.hasOne(Logins, { foreignKey: 'id' });
Nurses.hasOne(Teams, { foreignKey: 'nurse_id' });
Nurses.hasMany(Patients, { foreignKey: 'nurse_id' });
Patients.hasMany(NextOfKins, { foreignKey: 'patient_id' });
Patients.hasMany(PillDispensers, { foreignKey: 'patient_id' });
Doctors.hasMany(Patients, { foreignKey: 'doctor_id' });

// END: MYSQL RELATIONS

database
    .sync({force: config.seed})
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });


module.exports = {
    Logins: Logins,
    Patients: Patients,
    NextOfKin: NextOfKins,
    Nurses: Nurses,
    Teams: Teams,
    Doctors: Doctors,
    PillDispensers: PillDispensers
};


